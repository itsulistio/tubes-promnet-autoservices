<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Parts_model extends CI_Model {


	public function getParts()
	{
		$this->db->select('*');
		$this->db->from('parts');

		return $this->db->get()->result();
	}

	public function getPart($part_id)
	{
		$this->db->select('parts.code,parts.name,detail_parts.harga');
		$this->db->from('parts');
		$this->db->where('parts.id', $part_id);
		$this->db->join('detail_parts', 'detail_parts.part_id = parts.id');

		return $this->db->get()->result();
	}


	public function searchParts($part_name)
	{
		$this->db->select('parts.code,parts.name,detail_parts.harga');
		$this->db->from('parts');
		$this->db->where('parts.name', $part_name);
		$this->db->join('detail_parts', 'detail_parts.id_parts = parts.id');

		return $this->db->get()->result();
	}

	public function insert_part($table,$data)
	{
		
		$this->db->insert($table, $data);
		return $this->db->insert_id();

	}

	/**
	 * getPartsData function
	 * mengambil data kode, nama, harga
	 * 
	 * @access public
	 * @return object
	**/
	public function getPartsdata()
	{
		$this->db->select('parts.code,parts.name,detail_parts.price');
		$this->db->from('parts');
		$this->db->join('detail_parts', 'detail_parts.part_id = parts.id');

		return $this->db->get()->result();
	}

	/**
	 * getPartStock function
	 * menghitung jumlah stok berdasarkan tabel parts_serial
	 * 
	 * @access public
	 * @return integer
	 */
	public function getPartStock($part_id)
	{
		$this->db->where('part_id', $part_id);
		$this->db->from('detail_parts');
		return $this->db->count_all_results();
	}

	public function delete_part($id){
		$this->db->where('parts.id=detail_parts.id_parts');
    $this->db->where('detail_parts.id',$id);
    $this->db->delete(array('parts','detail_parts'));
	}
	
	public function update_stock($id){
		$this->db->where('id_parts',$id);
		$this->db->update('detail_parts');
	}
}

/* End of file parts_model.php */
/* Location: ./application/models/parts_model.php */