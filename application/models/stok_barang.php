<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
			
			<div class="list-group">
			    <a href="<?php echo base_url('index.php/admin') ?>" class="list-group-item list-group-item-action waves-effect">Halaman Utama</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Tambah Akun Kasir</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Rekapitulasi</a>
			    <a href="<?php echo base_url('index.php/admin/add_stock') ?>" class="list-group-item active waves-effect">Stok Barang</a>

			</div>
                
		</div> <!-- /.col-md -->

		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
				
			
				<div class="col-md-3"></div>
					<div class="col-md">
						<?php if ($this->session->flashdata('alert') != null): ?>
							<div class="alert alert-danger" role="alert">
								<?php echo $this->session->flashdata('alert'); ?>
							</div>
						<?php endif ?>
						<div class="row"> <!-- table row -->
						<div class="card card-body">
						<!--Table-->
						<table class="table table-hover">
					    <!--Table head-->
					    <thead class="mdb-color darken-3 text-white">
					    <?php foreach $data as $dat{ ?>
					        <tr>
					            <th>No</th>
					            <th>Code</th>
					            <th>Nama</th>
					            <th>Harga</th>
					            <th>Stok</th>
					            <th>Aksi</th>
					        </tr>
					    </thead>
					    <!--Table head-->

					    <!--Table body-->
					    <tbody>
					        <tr>
					            <th scope="row">1</th>
											<td>isi kolom 2</td>
											<td>isi kolom 3</td>
											<td>isi kolom 4</td>
											<td>isi kolom 5</td>
											<td><a class="btn btn-unique btn-sm" href="#">lihat</a> <a class="btn btn-danger btn-sm" href="#">hapus</a></td>
					        </tr>
					        <tr>
					            <th scope="row">1</th>
											<td>isi kolom 2</td>
											<td>isi kolom 3</td>
											<td>isi kolom 4</td>
											<td>isi kolom 5</td>
											<td><a class="btn btn-unique btn-sm" href="#">lihat</a> <a class="btn btn-danger btn-sm" href="#">hapus</a></td>
					        </tr>
					        <tr>
					            <th scope="row">1</th>
											<td>isi kolom 2</td>
											<td>isi kolom 3</td>
											<td>isi kolom 4</td>
											<td>isi kolom 5</td>
											<td><a class="btn btn-unique btn-sm" href="#">lihat</a> <a class="btn btn-danger btn-sm" href="#">hapus</a></td>
					        </tr>
					        <?php } ?>
					    </tbody>
					    <!--Table body-->

						</table>
						<!--Table-->

						</div>
					</div> <!-- /table row -->
				<div class="col-md-3"></div>
			</div>
			
		</div>
	</div> <!-- /row -->
</div> <!-- /container -->
		






				