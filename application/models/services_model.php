<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services_model extends CI_Model {

	public function getServices()
	{
		$this->db->select('*');
		$this->db->from('servis');

		return $this->db->get()->result();
	}

}

/* End of file services_model.php */
/* Location: ./application/models/services_model.php */