<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('parts_model');
	}

	public function index()
	{
		$this->load->view('header');
		$this->load->view('admin/home');
		$this->load->view('footer');
	}

	public function add_cashier()
	{
		if ($this->session->userdata('is_admin') == true) {
			$this->load->view('header');
			$this->load->view('admin/add_cashier');
			$this->load->view('footer');
		}else{
			$this->session->set_flashdata('alert', 'You were Logged Out');
			redirect('index.php/login','refresh');
		}
	}

	public function delete_parts(){

		$this->parts_model->delete_parts($id);
	}

	public function getPart(){

		$this->load->view('header');
		$this->load->view('admin/insert_parts');
		$this->load->view('footer');
		$data = $this->parts_model->getPart();
	}

	public function add_stock()
	{
		if ($this->session->userdata('is_admin') == true) {
			$this->load->view('header');
			$this->load->view('admin/insert_parts');
			$this->load->view('footer');
		}else{
			$this->session->set_flashdata('alert', 'You were Logged Out');
			redirect('index.php/login','refresh');
		}
	}

	public function add_cashier_process()
	{
		$username 	= $this->input->post('username');
		$password 	= $this->input->post('password');
		$nama				= $this->input->post('name');

		if ($this->session->userdata('is_admin') == TRUE) {
			if ($this->user_model->create_cashier($username,$password,$nama)) {
				$this->session->set_flashdata('alert', 'Berhasil menambahkan akun kasir');
			}else{
				$this->session->set_flashdata('alert', 'gagal menambahkan akun kasir');
			}

			redirect('index.php/admin/add_cashier','refresh');
		}else{
			redirect('index,php/login','refresh');
		}
	}
	public function add_data_parts()
	{
		$id		 	= '';
		$code	 	= $this->input->post('code');
		$name		= $this->input->post('name');
		$harga		= $this->input->post('harga');
		$stock		= $this->input->post('stock');

		if ($this->session->userdata('is_admin') == TRUE) {
			$data = array(
				'code' =>$code , 
				'name' =>$name , 
				);
			$id_parts = $this->parts_model->insert_part('parts',$data);

			$data1 = array(
				'id_parts' => $id_parts ,
				'harga' => $harga ,
				'stock' => $stock ,
				);
			$id_cek = $this->parts_model->insert_part('detail_parts',$data1);


			if ($id_cek != null) {
				$this->session->set_flashdata('alert', 'Berhasil menambahkan data stock');
			}else{
				$this->session->set_flashdata('alert', 'gagal menambahkan data stock');
			}

			redirect('index.php/admin/add_stock','refresh');
		}else{
			redirect('index,php/login','refresh');
		}
	}

	public function update_parts()
	{
		$id		 	= '';
		$code	 	= $this->input->post('code');
		$name		= $this->input->post('name');
		$harga		= $this->input->post('harga');
		$stock		= $this->input->post('stock');

		if ($this->session->userdata('is_admin') == TRUE) {
			$data = array(
				'code' =>$code , 
				'name' =>$name , 
				);
			$id_parts = $this->parts_model->update_stock('parts',$id);

			$data1 = array(
				'id_parts' => $id_parts ,
				'harga' => $harga ,
				'stock' => $stock ,
				);
			$id_cek = $this->parts_model->update_stock('detail_parts',$id1);
			


			if ($id_cek != null) {
				$this->session->set_flashdata('alert', 'Berhasil menambahkan data stock');
			}else{
				$this->session->set_flashdata('alert', 'gagal menambahkan data stock');
			}

			redirect('index.php/admin/add_stock','refresh');
		}else{
			redirect('index,php/login','refresh');
		}
	}
}
/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */
