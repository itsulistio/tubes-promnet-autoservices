<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="container"><br><br><br></div>
<div class="container">
	<div class="row">
		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>
		<div class="col-md-3"></div>
			<div class="col-md">
				<?php if (isset($this->session->flashdata('notif'))): ?>
					<div class="alert alert-danger" role="alert">
						<?php echo $this->session->flashdata('notif'); ?>
					</div>
				<?php endif ?>
				<div class="page-header">
					<h1>Tambah Admin</h1>
				</div>
				<hr>
				<?= form_open(base_url('index.php/login/create_admin')) ?>
					<div class="form-group">
						<label for="username" class="grey-text">Username</label>
						<input type="text" class="form-control" id="username" name="username" placeholder="">
					</div>
					<div class="form-group">
						<label for="password" class="grey-text">Password</label>
						<input type="password" class="form-control" id="password" name="password" placeholder="">
					</div>
					<div class="form-group">
						<label for="name" class="grey-text">Nama Lengkap</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="">
					</div>
					<div class="form-group">
						<label for="code" class="grey-text">kode khusus</label>
						<input type="password" class="form-control" id="code" name="code" placeholder="">
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-outline-default" value="Tambah">
						<a class="btn btn-outline-warning" href="<?php echo base_url('index.php/login') ?>">Kembali</a>
					</div>
				<?php form_close() ?>
				<hr>
			</div>
		<div class="col-md-3"></div>
	</div><!-- .row -->
</div><!-- .container -->
<div class="container"><br><br><br><br></div>