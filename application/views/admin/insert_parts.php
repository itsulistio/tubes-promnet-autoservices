<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-md-2">
			
			<div class="list-group">
			    <a href="<?php echo base_url('index.php/admin') ?>" class="list-group-item list-group-item-action waves-effect">Halaman Utama</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Tambah Akun Kasir</a>
			    <a href="#" class="list-group-item list-group-item-action waves-effect">Rekapitulasi</a>
			    <a href="<?php echo base_url('index.php/admin/add_stock') ?>" class="list-group-item active waves-effect">Stok Barang</a>

			</div>
                
		</div> <!-- /.col-md -->

		<?php if (validation_errors()) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= validation_errors() ?>
				</div>
			</div>
		<?php endif; ?>
		<?php if (isset($error)) : ?>
			<div class="col-md-12">
				<div class="alert alert-danger" role="alert">
					<?= $error ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="container">
			<div class="row">
				
			
				<div class="col-md-3"></div>
					<div class="col-md">
						<?php if ($this->session->flashdata('alert') != null): ?>
							<div class="alert alert-danger" role="alert">
								<?php echo $this->session->flashdata('alert'); ?>
							</div>
						<?php endif ?>
						<div class="page-header">
							<h1>Tambah Stok Barang</h1>
						</div>
						<hr>
						<?= form_open(base_url('index.php/admin/add_data_parts')) ?>
					<div class="form-group">
						<label for="code" class="grey-text">Code</label>
						<input type="text" class="form-control" id="code" name="code" placeholder="">
					</div>
					<div class="form-group">
						<label for="name" class="grey-text">Name</label>
						<input type="text" class="form-control" id="name" name="name" placeholder="">
					</div>
				
					<div class="form-group">
						<label for="harga" class="grey-text">Harga</label>
						<input type="text" class="form-control" id="harga" name="harga" placeholder="">
					</div>
					<div class="form-group">
						<label for="stock" class="grey-text">Stock</label>
						<input type="text" class="form-control" id="stock" name="stock" placeholder="">
					</div>
					<div class="form-group">
						<input type="submit" class="btn btn-outline-default" value="Tambah">
						<a class="btn btn-outline-warning" href="<?php echo base_url('index.php/login') ?>">Kembali</a>
					</div>
						<?php form_close() ?>
						<hr>
					</div>
				<div class="col-md-3"></div>
			</div>
			
		</div>
	</div> <!-- /row -->
</div> <!-- /container -->
